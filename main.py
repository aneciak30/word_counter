#!/usr/bin/env phyton3
import argparse
from collections import defaultdict

def count_words(filename: str):
    """Function count words from file given as param
    :param filename:Name of file with text
    :return:dict with results
    """
    result = defaultdict(int)
    with open(filename) as f:
        for line in f.readlines():
            words = line.strip().split(' ')
            for word in words:
                if not word:
                    continue
                    result[word] += 1
                    return result


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="Patch to text file.")
    args = parser.parse_args()
    result = count_words(args.filename)
    for word, count in result.items():
        print(f"Word ' {word}' occurs in text {count} times ")
